import socketio
import eventlet
import eventlet.wsgi
from flask import Flask, request, render_template
from flasgger import Swagger

from nameko.standalone.rpc import ClusterRpcProxy
from flasgger.utils import swag_from
import logging
import os
import json
sio = socketio.Server()

app = Flask(__name__)
Swagger(app)
app.debug = True
CONFIG = {'AMQP_URI': "pyamqp://guest:guest@localhost"}
# pyamqp://guest:guest@localhost


@app.route('/')
def index():
    # """Serve the client-side application."""
    return render_template('index.html')


@app.route('/send', methods=['POST'])
@swag_from('docs/send.yml')
def send():
    print("coba kirim")
    # logger = app.logger
    type = request.json.get('type')
    body = request.json.get('body')
    address = request.json.get('address')
    # logger.info('Get message send: %s,%s,%s' % (type, body, address))

    with ClusterRpcProxy(CONFIG) as rpc:
        # asynchronously spawning and email notification
        rpc.yowsup.send(type, body, address)

    msg = "The message was sucessfully sended to the queue"
    return msg, 200


@app.route('/terima', methods=['POST'])
def terima():
    # logger = app.logger
    print("ada pesan terima")
    print(request.headers)
    print(request.data)
    print(request.json)
    sio.emit('new_msg', request.json)
    return "OK", 200

# socket io server


@sio.on('connect', namespace='/chat')
def connect(sid, environ):
    print("connect ", sid)


@sio.on('chat message', namespace='/chat')
def message(sid, data):
    print("message ", data)
    sio.emit('new_msg', room=sid)


@sio.on('disconnect', namespace='/chat')
def disconnect(sid):
    print('disconnect ', sid)


if __name__ == "__main__":
    app = socketio.Middleware(sio, app)
    # deploy as an eventlet WSGI server
    eventlet.wsgi.server(eventlet.listen(('', 8001)), app)
